#include <iostream>
#include <vector>
#include <string>

using namespace std;


int search(string data, string key)
{
	int pos=0;
	
	for(unsigned int i = 0; i < data.size(); i++)
	{
		unsigned int j = i;
		for(unsigned int a = 0; a < key.size() && j < data.size(); a++)
		{
			if (data[j] != key[a])
			{
				break;
			}
			else
			{
				j++;
				if (a == 3)
				{
					pos = i;
					return pos;
				}
			}
		}
	}
	return -1;
}
	
